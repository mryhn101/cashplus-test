package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.model.Cart
import com.example.myapplication.databinding.CartItemBinding
import com.squareup.picasso.Picasso

class CartAdapter(
    private val onAddClick: (Cart) -> Unit,
    private val onMinusClick: (Cart) -> Unit,
    private val onDeleteClick: (Cart) -> Unit
) : RecyclerView.Adapter<CartAdapter.CartViewHolder>() {

    private var cartItems: List<Cart> = emptyList()

    fun setCartItems(items: List<Cart>) {
        cartItems = items
        notifyDataSetChanged()
    }

    inner class CartViewHolder(private val binding: CartItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(cartItem: Cart) {
            binding.tvCartTitle.text = cartItem.product.title
            val formattedCartPrice = String.format("%.2f", cartItem.product.price)
            binding.tvCartPrice.text = "$$formattedCartPrice"
            binding.tvAmount.text = cartItem.quantity.toString()
            val formatTotalPrice = String.format("%.2f", cartItem.totalItemPrice)
            binding.tvTotalPrice.text = "$$formatTotalPrice"
            Picasso.get().load(cartItem.product.image).into(binding.ivCartImage)

            binding.icAdd.setOnClickListener {
                onAddClick(cartItem)
            }

            binding.icMinus.setOnClickListener {
                onMinusClick(cartItem)
            }
            binding.icDelete.setOnClickListener {
                onDeleteClick(cartItem)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CartViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = CartItemBinding.inflate(inflater, parent, false)
        return CartViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return cartItems.size
    }

    override fun onBindViewHolder(holder: CartViewHolder, position: Int) {
        val cartItem = cartItems[position]
        holder.bind(cartItem)
    }
}
