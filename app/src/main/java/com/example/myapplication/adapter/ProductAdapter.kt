package com.example.myapplication.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.ui.product_detail.ProductDetailActivity
import com.example.myapplication.data.model.Product
import com.example.myapplication.databinding.ProductItemBinding
import com.squareup.picasso.Picasso

class ProductAdapter(
    private var c: Context,
    private var item: List<Product>
): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    class ViewHolder(val itemBinding: ProductItemBinding): RecyclerView.ViewHolder(itemBinding.root )

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =ProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)


        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val items = item[position]
        val detailItem = item[position]

        Picasso.get().load(items.image).into(holder.itemBinding.ivThumbnail)
        holder.itemBinding.tvTitle.text = items.title
        holder.itemBinding.tvPrice.text = "$${items.price}"

        holder.itemView.setOnClickListener {
            val image = detailItem.image
            val title = detailItem.title
            val price = detailItem.price
            val id = detailItem.id

            val intent = Intent(c, ProductDetailActivity::class.java)
            intent.putExtra("id", id)
            intent.putExtra("title", title)
            intent.putExtra("price", price)
            intent.putExtra("image", image)
            c.startActivity(intent)
        }


    }
}