package com.example.myapplication.data.db.cart

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface CartDao {
    @Query("SELECT * FROM CartEntities")
    suspend fun getAll(): List<CartEntities>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(cartEntities: CartEntities)

    @Delete
    suspend fun delete(cartEntities: CartEntities)

    @Query("SELECT * FROM CartEntities WHERE productId = :productId")
    suspend fun getCartItem(productId: Int): CartEntities?

    @Query("DELETE FROM cartentities WHERE productId = :productId")
    suspend fun deleteCartItem(productId: Int)
}