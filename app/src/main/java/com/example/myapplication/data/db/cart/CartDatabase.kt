package com.example.myapplication.data.db.cart

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [CartEntities::class], version = 2)
abstract class CartDatabase: RoomDatabase() {
    abstract fun cartDao(): CartDao
}