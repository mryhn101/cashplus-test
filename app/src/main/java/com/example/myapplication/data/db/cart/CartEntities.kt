package com.example.myapplication.data.db.cart

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CartEntities(
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val productId: Int,
    val quantity: Int,
    val totalItemPrice: Double
)