package com.example.myapplication.data.db.product

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface ProductDao {
    //Products
    @Query("SELECT * FROM ProductEntities")
    suspend fun getAll(): List<ProductEntities>

    @Query("SELECT * FROM ProductEntities WHERE title LIKE :term")
    fun search(term: String): List<ProductEntities>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(product:  List<ProductEntities>)

    @Query("SELECT * FROM ProductEntities WHERE id = :productId")
    suspend fun getProductById(productId: Int): ProductEntities?
}