package com.example.myapplication.data.db.product

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ProductEntities::class], version = 2)
abstract class ProductDatabase: RoomDatabase() {
    abstract fun productDao(): ProductDao
}