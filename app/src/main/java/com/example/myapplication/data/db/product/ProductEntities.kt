package com.example.myapplication.data.db.product

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class ProductEntities(
    @PrimaryKey(autoGenerate = true)
    val id: Int?,
    val title: String,
    val image: String,
    val price: Double,

)