package com.example.myapplication.data.model

data class Cart(
    val product: Product,
    val quantity: Int,
    val totalItemPrice: Double
)