package com.example.myapplication.data.model

data class Product(
    var id: Int,
    var title: String,
    var image: String,
    var price: Double,
)