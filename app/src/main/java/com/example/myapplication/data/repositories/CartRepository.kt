package com.example.myapplication.data.repositories

import com.example.myapplication.data.db.cart.CartDao
import com.example.myapplication.data.db.cart.CartEntities
import com.example.myapplication.data.model.Cart
import com.example.myapplication.data.model.Product

class CartRepository(private val cartDao: CartDao, private val productRepository: ProductRepository) {

    suspend fun addToCart(product: Product, quantity: Int) {

        val existingCart = cartDao.getCartItem(product.id)

        if (existingCart != null) {
            val updatedCart = existingCart.copy(quantity = existingCart.quantity + quantity )
            cartDao.insert(updatedCart)
        } else {
            val cartEntities = CartEntities(productId = product.id, quantity = quantity, totalItemPrice = product.price*quantity)
            cartDao.insert(cartEntities)
        }
    }

    suspend fun reduceFromCart(product: Product, quantity: Int) {
        val existingCart = cartDao.getCartItem(product.id)

        if (existingCart!!.quantity > 1) {
            val updatedCart = existingCart.copy(quantity = existingCart.quantity - quantity )
            cartDao.insert(updatedCart)
        }
    }

    suspend fun getCartItems(): List<Cart> {
        val cartEntitiesList = cartDao.getAll()
        return cartEntitiesList.map {
            Cart(product = productRepository.getProductById(it.productId), quantity = it.quantity, totalItemPrice = productRepository.getProductById(it.productId).price*it.quantity)
        }
    }

    suspend fun removeCart(product: Product){
        val existingCart = cartDao.getCartItem(product.id)
        cartDao.deleteCartItem(existingCart!!.productId)
    }

    suspend fun clearCart() {
        cartDao.getAll().forEach { cartDao.delete(it) }
    }
}