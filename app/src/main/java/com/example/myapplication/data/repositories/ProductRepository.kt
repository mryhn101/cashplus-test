package com.example.myapplication.data.repositories

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import com.example.myapplication.data.db.product.ProductDao
import com.example.myapplication.data.db.product.ProductEntities
import com.example.myapplication.data.model.Product
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.net.URL

class ProductRepository(private val productDao: ProductDao) {

    suspend fun getProducts(context: Context): List<Product> {

        try {
            return if (isNetworkAvailable(context)) {

                val json = withContext(Dispatchers.IO) {
                    URL("https://fakestoreapi.com/products").readText()
                }
                val productsFromApi = Gson().fromJson(json, Array<Product>::class.java).toList()

                saveProductsToDatabase(productsFromApi)
            } else {
                getFromDb()
            }
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d("MainActivity", "${e.printStackTrace()}}")
            return emptyList()
        }
    }

    suspend fun getProductById(productId: Int): Product {
        return withContext(Dispatchers.IO) {
            val productEntity = productDao.getProductById(productId)
            return@withContext productEntity?.toProduct() ?: throw NoSuchElementException("Product not found")
        }
    }

    suspend fun searchProducts(query: String): List<Product>{
        return withContext(Dispatchers.IO){
            val searchResults = productDao.search("%$query%")
            return@withContext searchResults.map { it.toProduct() }
        }
    }

    private suspend fun saveProductsToDatabase(products: List<Product>): List<Product> {
        val productEntities = products.map { it.toProductEntities() }

        productDao.insertAll(productEntities)

        return getFromDb()
    }

    private suspend fun getFromDb(): List<Product> {
        val productEntitiesFromDb = productDao.getAll()
        return productEntitiesFromDb.map { it.toProduct() }
    }

    private fun isNetworkAvailable(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val network = connectivityManager.activeNetwork
        val capabilities = connectivityManager.getNetworkCapabilities(network)
        return capabilities != null && (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || capabilities.hasTransport(
            NetworkCapabilities.TRANSPORT_CELLULAR
        ))
    }

    private fun Product.toProductEntities(): ProductEntities {
        return ProductEntities(id, title, image, price)
    }

    private fun ProductEntities.toProduct(): Product {
        return Product(id!!, title, image, price)
    }

}