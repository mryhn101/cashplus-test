package com.example.myapplication.di

import androidx.room.Room
import com.example.myapplication.data.db.cart.CartDatabase
import com.example.myapplication.data.db.product.ProductDatabase
import com.example.myapplication.data.repositories.CartRepository
import com.example.myapplication.data.repositories.ProductRepository
import com.example.myapplication.ui.cart.CartViewModel
import com.example.myapplication.ui.product.ProductViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            ProductDatabase::class.java, "product-database"
        ).build()
    }
    single {
        get<ProductDatabase>().productDao()
    }
    single { ProductRepository(get()) }
    viewModel { ProductViewModel(get()) }

    single {
        Room.databaseBuilder(
            androidApplication(),
            CartDatabase::class.java, "cart-database"
        ).build()
    }
    single {
        get<CartDatabase>().cartDao()
    }
    single { CartRepository(get(), get()) }
    viewModel { CartViewModel(get()) }

}

