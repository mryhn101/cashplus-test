package com.example.myapplication.ui.cart

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.adapter.CartAdapter
import com.example.myapplication.databinding.ActivityCartBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class CartActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCartBinding
    private val cartViewModel: CartViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val cartAdapter = CartAdapter(
            onAddClick = {
                cartViewModel.addToCart(it.product, 1)
            },
            onMinusClick = {
                cartViewModel.reduceFromCart(it.product, 1)
            },
            onDeleteClick = {
                cartViewModel.removeCart(it.product)
                Toast.makeText(this@CartActivity, "Product removed from cart", Toast.LENGTH_SHORT).show()

            }
        )

        binding.btnOrder.setOnClickListener {
            cartViewModel.clearCart()
            Toast.makeText(this@CartActivity, "Thanks for ordering", Toast.LENGTH_SHORT).show()

        }

        binding.recyclerView.apply {
            layoutManager = LinearLayoutManager(this@CartActivity)
            adapter = cartAdapter
        }

        cartViewModel.cartItems.observe(this) {
            cart -> cartAdapter.setCartItems(cart)
        }

        cartViewModel.totalCartPrice.observe(this) {
            val formatPrice = String.format("%.2f", it)
            binding.tvOverallPrice.text = "Total Price $$formatPrice"
        }
    }
}