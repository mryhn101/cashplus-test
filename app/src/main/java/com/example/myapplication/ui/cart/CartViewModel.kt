package com.example.myapplication.ui.cart

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.myapplication.data.model.Cart
import com.example.myapplication.data.repositories.CartRepository
import androidx.lifecycle.viewModelScope
import com.example.myapplication.data.model.Product
import kotlinx.coroutines.launch

class CartViewModel(private val cartRepository: CartRepository): ViewModel() {
    private val _cartItems = MutableLiveData<List<Cart>>()
    val cartItems: LiveData<List<Cart>> get() = _cartItems

    private val _totalCartPrice = MediatorLiveData<Double>()
    val totalCartPrice: LiveData<Double> get() = _totalCartPrice

    init {
        viewModelScope.launch {
            refreshCartItems()
        }

        _totalCartPrice.addSource(_cartItems) {
            calculateTotalPrice()
        }
    }

    fun addToCart(product: Product, quantity: Int) {
        viewModelScope.launch {
            cartRepository.addToCart(product, quantity)
            refreshCartItems()
        }
    }

    fun reduceFromCart(product: Product, quantity: Int) {
        viewModelScope.launch {
            cartRepository.reduceFromCart(product, quantity)
            refreshCartItems()
        }
    }

    fun removeCart(product: Product){
        viewModelScope.launch {
            cartRepository.removeCart(product)
            refreshCartItems()
        }
    }

    private suspend fun refreshCartItems() {
        val cartItems = cartRepository.getCartItems()
        _cartItems.postValue(cartItems)
    }

    fun clearCart() {
        viewModelScope.launch {
            cartRepository.clearCart()
            refreshCartItems()
        }
    }

    private fun calculateTotalPrice() {
        val cartItems = _cartItems.value
        if (cartItems != null) {
            val total = cartItems.sumOf { it.totalItemPrice }
            _totalCartPrice.value = total
        }
    }
}