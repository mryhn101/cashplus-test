package com.example.myapplication.ui.product

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.room.Room
import com.example.myapplication.R
import com.example.myapplication.adapter.ProductAdapter
import com.example.myapplication.data.db.product.ProductDatabase
import com.example.myapplication.data.repositories.ProductRepository
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.ui.cart.CartActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val productViewModel: ProductViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val productDb = Room.databaseBuilder(
            applicationContext,
            ProductDatabase::class.java, "product-database"
        ).build()

        val productDao = productDb.productDao()

        ProductRepository(productDao)

        productViewModel.fetchData(this@ProductActivity)

        productViewModel.products.observe(this) { products ->
            binding.recyclerView.apply {
                layoutManager = LinearLayoutManager(this@ProductActivity)
                adapter = ProductAdapter(context, products)
            }
        }

        binding.btnSearch.setOnClickListener {
            productViewModel.searchProducts(binding.etSearch.text.toString())
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        startActivity(Intent(this, CartActivity::class.java))
        return true
    }

}