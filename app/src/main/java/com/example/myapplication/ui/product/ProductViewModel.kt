package com.example.myapplication.ui.product

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.data.model.Product
import com.example.myapplication.data.repositories.ProductRepository
import kotlinx.coroutines.launch

class ProductViewModel(private val repository: ProductRepository): ViewModel() {
    private val _products = MutableLiveData<List<Product>>()
    val products: LiveData<List<Product>> get() = _products

    fun fetchData(context: Context){
        viewModelScope.launch {
            try {
                _products.value = repository.getProducts(context)
            } catch(e: Exception){
                e.printStackTrace()
            }
        }
    }

    fun searchProducts(query: String) {
        viewModelScope.launch {
            try {
                val searchResults = repository.searchProducts(query)
                _products.value = searchResults
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}