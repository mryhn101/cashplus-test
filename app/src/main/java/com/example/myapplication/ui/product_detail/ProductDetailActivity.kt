package com.example.myapplication.ui.product_detail

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.data.model.Product
import com.example.myapplication.databinding.ActivityProductDetailBinding
import com.example.myapplication.ui.cart.CartViewModel
import com.squareup.picasso.Picasso
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProductDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductDetailBinding
    private val viewModel: CartViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.tvTitleDetail.text = intent.getStringExtra("title")
        Picasso.get().load(intent.getStringExtra("image")).into(binding.ivThumbnailDetail)
        val price = intent.getDoubleExtra("price", 0.0).toString()
        val formattedPrice = "$$price"
        binding.tvPriceDetail.text = formattedPrice

        binding.btnAddToCart.setOnClickListener {
            val currentProduct = Product(
                id = intent.getIntExtra("id", 1),
                title = intent.getStringExtra("title") ?: "",
                image = intent.getStringExtra("image") ?: "",
                price = intent.getDoubleExtra("price", 0.0)
            )
            viewModel.addToCart(currentProduct, 1)

            Toast.makeText(this@ProductDetailActivity, "Product added to cart", Toast.LENGTH_SHORT).show()
        }
    }
}